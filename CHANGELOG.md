# Changelog

### 1.0.8

* Fixed: Update error handler to support PHP 8

### 1.0.7

* Added: Ability to set S3 url to append to getUrl() result, rather than returning simply the path

### 1.0.6

* Fixed:    Fix for issue with deleteAsset not working due to s3 streamwrappers not being attached

### 1.0.5

* Added:    S3AssetException
* Updated:  Updated S3StaticCatalogueProvider to throw an exception when an error is detected in the AWS SDK

### 1.0.4

* Fixed codeception/verify to require-dev

### 1.0.3

* Added:    S3StaticCatalogueProvider

### 1.0.2

* Added:    Logging

### 1.0.1

* Fixed:    PHP 5.6 support

### 1.0.0

* Added:    First version
