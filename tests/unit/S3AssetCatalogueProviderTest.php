<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\AmazonS3AssetCatalogueProvider\Tests;

use Gcdtech\AmazonS3AssetCatalogueProvider\S3AssetCatalogueProvider;
use Gcdtech\AmazonS3AssetCatalogueProvider\S3AssetCatalogueProviderSettings;
use Gcdtech\Aws\Settings\AwsSettings;
use Rhubarb\Crown\Assets\AssetCatalogueSettings;
use Rhubarb\Crown\Exceptions\AssetExposureException;
use Rhubarb\Crown\Tests\unit\Assets\AssetCatalogueProviderTests;

class S3AssetCatalogueProviderTest extends AssetCatalogueProviderTests
{
    protected function getProvider()
    {
        $aws = AwsSettings::singleton();
        $aws->region = "eu-west-1";
        $aws->sslVerify = false;

        $settings = S3AssetCatalogueProviderSettings::singleton();
        $settings->bucket = "rhubarbphp";

        return new S3AssetCatalogueProvider("rhubarb-test");
    }

    public function testGetUrl()
    {
        $settings = AssetCatalogueSettings::singleton();
        $settings->jwtKey = "rhubarbphp";

        $content = uniqid();
        $file = __DIR__.'/test.txt';
        file_put_contents($file, $content);

        $provider = $this->getProvider();
        $asset = $provider->createAssetFromFile($file, ["category" => "rhubarb-test"]);

        try {
            $asset->getUrl();
            $this->fail("We haven't public mappings yet");
        } catch (AssetExposureException $er){
        }

        $s3Settings = S3AssetCatalogueProviderSettings::singleton();
        $s3Settings->categoryUrlMap["rhubarb-test"] = "http://dukxqoq2ov9yy.cloudfront.net/rhubarb-test/";

        try {
            $url = $asset->getUrl();
            $urlContent = file_get_contents($url);

            $this->assertEquals($content, $urlContent);
        } catch (AssetExposureException $er){
            $this->fail("Now the mapping is there we should be able to fetch this.");
        }
    }
}