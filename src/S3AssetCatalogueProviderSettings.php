<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\AmazonS3AssetCatalogueProvider;

use Rhubarb\Crown\Settings;

class S3AssetCatalogueProviderSettings extends Settings
{
    /**
     * The name of a pre-existing bucket in S3
     *
     * @var string
     */
    public $bucket = "";

    /**
     * A map of categories (keys) to url stubs
     *
     * @var array
     */
    public $categoryUrlMap = [];

    /**
     * Full domain to the URL - used in the providers 'getUrl' method, WITHOUT trailing slash
     *  e.g. https://s3-eu-west-1.amazonaws.com/<bucket>
     *
     * @var string
     */
    public $bucketUrl = '';
}