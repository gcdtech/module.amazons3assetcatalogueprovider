<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\AmazonS3AssetCatalogueProvider;

use Aws\S3\S3Client;
use Gcdtech\Aws\Settings\AwsSettings;
use Rhubarb\Crown\Assets\Asset;
use Rhubarb\Crown\Assets\AssetCatalogueProvider;
use Rhubarb\Crown\Exceptions\AssetException;
use Rhubarb\Crown\Exceptions\AssetExposureException;
use Rhubarb\Crown\Exceptions\AssetNotFoundException;
use Rhubarb\Crown\Exceptions\SettingMissingException;
use Rhubarb\Crown\Logging\Log;

/**
 * An AssetCatalogueProvider that stores files in S3
 */
class S3AssetCatalogueProvider extends AssetCatalogueProvider
{
    /**
     * Used to check if the error being reported came from the Amazon SDK
     * @var string
     */
    const AWS_SDK_VENDOR_PACKAGE_PATH = 'aws-sdk-php/';

    /**
     * Used to track if the error handler has already been set for this Provider
     * This will prevent multiple handlers being defined.
     * @var bool
     */
    private static $errorHandlerDefined = false;

    /** @var S3Client $client */
    private $client = null;

    /**
     * S3AssetCatalogueProvider constructor.
     * @param string $category
     */
    public function __construct($category = "")
    {
        parent::__construct($category);
        self::registerErrorsHandler();
    }

    protected function getClient() : S3Client
    {
        if(!$this->client instanceof S3Client) {
            $settings = AwsSettings::singleton();
            $this->client = new S3Client($settings->getClientSettings());
        }

        return $this->client;
    }

    /**
     * Registers the s3:// stream wrappers which we rely on for file transport.
     * @throws SettingMissingException
     */
    private function registerStreamWrappers()
    {
        $this->getClient()->registerStreamWrapper();
    }

    /**
     * Makes the category name safe to use for a file path but guaranteed unique.
     *
     * e.g. This! and This@ should both work.
     */
    private function getCategoryDirectory()
    {
        if (!$this->category){
            return "_default";
        }

        return $this->category;
    }

    protected function getObjectName($filePath)
    {
        $uniqueString = uniqid();
        $uniqueFolder = substr($uniqueString, -2);
        $uniquePrefix = substr($uniqueString, 0, -2);
        return $uniqueFolder . "/" . $uniquePrefix . "_" . basename($filePath);
    }

    public function createAssetFromFile($filePath, $commonProperties)
    {
        $this->registerStreamWrappers();

        $settings = S3AssetCatalogueProviderSettings::singleton();
        $bucket = $settings->bucket;

        if (!$bucket){
            throw new SettingMissingException("S3AssetCatalogueProviderSettings", "bucket");
        }

        $newName = $this->getObjectName($filePath);

        $categoryDirectory = $this->getCategoryDirectory();

        $read = fopen($filePath, "r");

        if (!$read){
            Log::error("Could not open local file '$filePath'' for reading.", "ASSETS");
            throw new AssetException("", "Could not open local file '$filePath'' for reading.");
        }

        $write = fopen("s3://".$bucket."/".$categoryDirectory."/".$newName, "w");

        if (!$write){
            Log::error("S3AssetCatalogueProvider could not create stream. Check AWS credentials.", "ASSETS");
            throw new AssetException("S3AssetCatalogueProvider could not create stream. Check AWS credentials.");
        }

        while(!feof($read)){
            $bytes = fread($read, 8192);

            if ($bytes !==false ){
                fwrite($write, $bytes);
            }
        }

        fclose($read);
        fclose($write);

        Log::debug("S3 asset 's3://".$bucket."/".$categoryDirectory."/".$newName."' created", "ASSETS");

        // Create the token.
        $commonProperties["file"] = $newName;

        $token = $this->createToken($commonProperties);
        $asset = new Asset($token, $this, $commonProperties);

        Log::debug("S3 token created '$token'", "ASSETS");

        return $asset;
    }

    public function getStream(Asset $asset)
    {
        $this->registerStreamWrappers();

        $settings = S3AssetCatalogueProviderSettings::singleton();
        $bucket = $settings->bucket;

        if (!$bucket){
            throw new SettingMissingException("S3AssetCatalogueProviderSettings", "bucket");
        }

        $categoryDirectory = $this->getCategoryDirectory();

        // Get the file name from the provider data
        $data = $asset->getProviderData();

        $path = "s3://".$bucket."/".$categoryDirectory."/".$data["file"];

        if (!file_exists($path)){
            throw new AssetNotFoundException($asset->getToken());
        }

        $handle = fopen($path, "r");

        if (!$handle){
            throw new AssetException("S3AssetCatalogueProvider could not create stream. Check AWS credentials.");
        }

        return $handle;
    }

    public function getUrl(Asset $asset)
    {
        $settings = S3AssetCatalogueProviderSettings::singleton();
        $data = $asset->getProviderData();

        if (!isset($settings->categoryUrlMap[$data["category"]])) {
            throw new AssetExposureException($asset->getToken(), null);
        }

        $key = '/' . trim($settings->categoryUrlMap[$data["category"]], '/').'/'.$data["file"];
        if(empty($settings->bucketUrl)){
            return $key;
        }
        return rtrim($settings->bucketUrl, '/') . $key;
    }

    /**
     * Deletes the given asset
     *
     * @param Asset $asset
     * @return mixed
     * @throws AssetNotFoundException
     * @throws SettingMissingException
     */
    public function deleteAsset(Asset $asset)
    {
        $this->registerStreamWrappers();

        $settings = S3AssetCatalogueProviderSettings::singleton();
        $bucket = $settings->bucket;

        if (!$bucket){
            throw new SettingMissingException("S3AssetCatalogueProviderSettings", "bucket");
        }

        $categoryDirectory = $this->getCategoryDirectory();

        // Get the file name from the provider data
        $data = $asset->getProviderData();

        $path = "s3://".$bucket."/".$categoryDirectory."/".$data["file"];

        if (!file_exists($path)){
            throw new AssetNotFoundException($asset->getToken());
        }

        unlink($path);
    }

    /**
     * This was added to trap any errors published by the Amazon S3 PHP SDK and throw an appropriate exception.
     * It was needed due to their SDK not publishing exceptions instead they trigger errors which meant that should
     * any error occur the Provider would continue and error would not be caught.
     */
    protected static function registerErrorsHandler()
    {
        if (!self::$errorHandlerDefined) {
            set_error_handler(function ($errorNumber, $errorMessage, $errorFile, $errorLine, $errorContext = null) {
                if (stripos($errorFile, self::AWS_SDK_VENDOR_PACKAGE_PATH) !== false) {
                    $exception = new S3AssetException("Unexpected error occurred when uploading to S3AssetCatalogueProvider");
                    $exception->setErrorNumber($errorNumber);
                    $exception->setErrorMessage($errorMessage);
                    $exception->setErrorFile($errorFile);
                    $exception->setErrorLineNumber($errorLine);

                    throw $exception;
                }
            });

            self::$errorHandlerDefined = true;
        }
    }

    /**
     * Used to reset the error handler that is added to trap errors generated by the Amazon S3 PHP SDK
     */
    protected static function resetErrorsHandler()
    {
        set_error_handler(null);
    }
}
