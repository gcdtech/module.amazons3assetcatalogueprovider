<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\AmazonS3AssetCatalogueProvider;

use Rhubarb\Crown\Exceptions\AssetException;

/**
 * An Exception used to capture error information reported by the Amazon S3 PHP SDK
 */
class S3AssetException extends AssetException
{
    private $errorNumber;
    private $errorMessage;
    private $errorFile;
    private $errorLineNumber;

    public function getErrorLineNumber()
    {
        return $this->errorLineNumber;
    }

    public function setErrorLineNumber($errorLineNumber)
    {
        $this->errorLineNumber = $errorLineNumber;
    }

    public function getErrorNumber()
    {
        return $this->errorNumber;
    }

    public function setErrorNumber($errorNumber)
    {
        $this->errorNumber = $errorNumber;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return mixed
     */
    public function getErrorFile()
    {
        return $this->errorFile;
    }

    public function setErrorFile($errorFile)
    {
        $this->errorFile = $errorFile;
    }

    public function getSummaryErrorMessage()
    {
        $errorMessage = '';

        if ($this->errorFile) {
            $errorMessage .= "Error Detected in file: " . $this->errorFile;
        }

        if ($this->errorMessage) {
            $errorMessage .= " Error Message: $this->errorMessage";
        }

        if ($this->errorNumber) {
            $errorMessage .= " Error Number: $this->errorNumber";
        }

        if ($this->errorLineNumber) {
            $errorMessage .= " Error Line Number: $this->errorLineNumber";
        }

        return $errorMessage;
    }
}
