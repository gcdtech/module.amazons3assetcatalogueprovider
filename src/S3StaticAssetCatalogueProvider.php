<?php

namespace Gcdtech\AmazonS3AssetCatalogueProvider;

/**
 * A S3AssetCatalogueProvider that does not store files as the name passed through, rather than a unique name each time
 *
 * Class S3StaticAssetCatalogueProvider
 * @package Gcdtech\AmazonS3AssetCatalogueProvider
 */
class S3StaticAssetCatalogueProvider extends S3AssetCatalogueProvider
{
    protected function getObjectName($filePath)
    {
        return basename($filePath);
    }
}